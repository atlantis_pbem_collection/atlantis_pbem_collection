
# Atlantis PBeM Collection

We try to collect as many Atlantis-related play-by-email game hosts as we can find and archive them here.




## Collected


### [Atlantis 1](https://gitlab.com/atlantis_pbem_collection/atlantis1_pbem)

created by Russell Wallace.

found at: http://www.prankster.com/project/docs/history.htm


### [Atlantis 2](https://gitlab.com/atlantis_pbem_collection/atlantis2_pbem)

successor of Atlantis 1.

created by Russell Wallace.

found at: http://www.prankster.com/project/docs/history.htm

source is known to be incomplete and won't compile.


### Atlantis 3

no source available. nothing to collect.

references:
- [Rules](http://www.prankster.com/old/atl3/rules_30.html)
- [Website](http://www.prankster.com/old/atl3.html)


### Atlantis 4

#### [Atlantis 4.0.0 - 4.0.4](https://gitlab.com/atlantis_pbem_collection/atlantis4_pbem)

found at: http://www.prankster.com/project/download/download.htm
  
created by Geoff Dunbar.

contains version 4.0.0 to 4.0.4 as *.tar.gz-files.

#### [Atlantis 4.0.4+ - 5.1.0](https://gitlab.com/atlantis_pbem_collection/atlantis5_pbem)

found at: https://github.com/Atlantis-PBEM/Atlantis
  
contains version 4.0.4 to 5.1.0, but version 4.0.4 differs to Geoff Dunbars version 4.0.4.
  
#### other older Atlanits 4 files

- found at: https://sourceforge.net/projects/atlantis/
  
  older repo. is completely included in github-repo.

- found at: https://groups.yahoo.com/neo/groups/ATLANTISDEV/files/Sources/
  
  lots of .tar.gz and diff-files for versions 4.0.8 to 5.1.0

  These are included in the github-repo. Or more probably these were created from the github-repo.


### [Anduna](https://gitlab.com/atlantis_pbem_collection/anduna_pbem)

found at: https://sourceforge.net/projects/atlantispbem/

won't compile: spells.c is missing!

based on: GermanAtlantis version 5.3

rules are [here](http://shyguy.homeip.net/)


### [XAtlantis](https://gitlab.com/atlantis_pbem_collection/xatlantis_pbem) (aka: GermanAtlantis 6.6)

additionally archived:
- [command checker](https://gitlab.com/atlantis_pbem_collection/xatlantis_checker)

found at: https://sourceforge.net/projects/xatlantis/

based on: GermanAtlantis version 6.2




## Found but not yet collected (ToDo)

### GermanAtlantis

found at:
- https://github.com/kensanata/germanatlantis

- https://gitlab.com/german-atlantis/german-atlantis


### Fantasya


### Olympia

was inspiration for Atlantis 1.


### Olympia G3

found at: https://sourceforge.net/projects/olympiag3/

reference: 
- http://www.shadowlandgames.com/olympia/
- https://github.com/Atlantis-PBEM/Atlantis/issues/24


### Havilah

refence: http://www.shadowlandgames.com/havilah/


### Lorenai

found at: https://sourceforge.net/projects/lorenai/

Not sure, if this is related to Atlantis...

### Eressea


## Lost

Following games are mentioned somewhere, but all links are dead.

references:
- https://alexschroeder.ch/wiki/AtlantisGames
- http://pygma-isis.de/spiele.html
- http://www.studis.de/Eressea/Links/spiele.html


### Allanon / Menouthis

based on GermanAtlantis.

was rewritten in Java.


### Arcadia

based on Atlantis 5


### Eidolon

base on GermanAtlantis 5.3


### Olympia G3

based on Olympia


### Panghea

based on GermanAtlantis 6.2


### Rorqual

based on GermanAtlantis 4

was rewritten?

### Sitanleta

based on GermanAtlantis 6.2

switched to hex-map


### Verdanon

based on GermanAtlantis

switched to hex-map


### Vinyambar


### Empiria


### Muranien


### Lacandón Conquest



## Miscellaneous

unsorted unevaluated stuff..

- https://github.com/Atlantis-PBEM/Atlantis-Tools

- https://github.com/ennorehling/atlantis-movement

- https://github.com/ennorehling/alh

  Atlantis Little Helper <- new search term

- https://sourceforge.net/projects/alh/

- https://github.com/Labutin/DSNAtlantis

- https://github.com/essenbee/atlantis

- https://github.com/AtlaClient/AtlaClient

  AtlaClient <- new search term; some commits mention cvsignore.. search sourceforge

- https://github.com/AtlaClient/AtlaClient-old

- https://sourceforge.net/projects/magellan-client/

- https://sourceforge.net/projects/overlord/

- https://sourceforge.net/projects/zugator/

- https://sourceforge.net/projects/ceraan/

- https://sourceforge.net/projects/tarmellion/


